from audioop import reverse
from email import contentmanager
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView

from todos.models import TodoList, TodoItem

# Create your views here.


class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"
    # the context_object_name tells the for loop in the 
    # cooresponding template what to do
    context_object_name = "total_todos"

class TodoDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"
    #context_object_name = "todos_detail_view"

class TodoCreateView(CreateView):
    model = TodoList
    template_name = "todos/new.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todos_list", args=[self.object.id])
    
    # this is what I had before and I was not passing 
    # test for when the form is submitted with an empty 
    # value for "name", then the form is no valid and 
    # the returing page does not redirect

    # success_url = reverse_lazy("todos_list")

class TodoUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]
    # success_url = reverse_lazy("todos_list")
    def get_success_url(self):
        return reverse_lazy("todos_detail", args=[self.object.id])

class TodoDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todos_list")

class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/newitem.html"
    fields = ["task", "due_date", "is_completed", "list"]
    success_url = reverse_lazy("todos_list")
    context_object_name = "total_items"


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/edititem.html"
    fields = ["task", "due_date", "is_completed", "list"] 
    success_url = reverse_lazy("todos_list")